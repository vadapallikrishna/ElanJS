

ELANJS - JAVASCRIPT ANIMATION LIBRARY
-----------------------------------------------------------------------------------------------



ElAN - Element animator
It allows you to animate any object using web animations api
it uses all predefined functions from google animate.css implementation


The functions are

	1) animate(selector, animation, iterations);
		selector can be any class ,id, tag and events can be specified for any element as selector:event

	2) move(selector, dirs)
		dirs are left,right,top,bottom
		it simply uses css to make element's position absolute
		and move object
